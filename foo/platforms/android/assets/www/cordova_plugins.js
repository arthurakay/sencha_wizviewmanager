cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/jp.wizcorp.phonegap.plugin.wizViewManagerPlugin/www/phonegap/plugin/wizViewManager/wizViewManager.js",
        "id": "jp.wizcorp.phonegap.plugin.wizViewManagerPlugin.wizViewManagerPlugin",
        "clobbers": [
            "window.wizViewManager"
        ]
    },
    {
        "file": "plugins/jp.wizcorp.phonegap.plugin.wizViewManagerPlugin/www/phonegap/plugin/wizViewManager/wizViewMessenger.js",
        "id": "jp.wizcorp.phonegap.plugin.wizViewManagerPlugin.wizViewMessenger",
        "clobbers": [
            "window.wizViewMessenger"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "jp.wizcorp.phonegap.plugin.wizViewManagerPlugin": "1.3.0"
}
// BOTTOM OF METADATA
});