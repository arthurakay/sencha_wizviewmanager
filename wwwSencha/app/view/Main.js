var IS_CREATED = false,
    INTERVAL_ID;

function hideChildWindow() {
    clearTimeout(INTERVAL_ID);

    wizViewManager.hide('MAIN', {});
}

function showChildWindow() {
    wizViewManager.show('MAIN', {});

    Ext.Viewport.unmask();

    INTERVAL_ID = setTimeout(hideChildWindow, 5000);
}

function createChildWindow() {
    if (IS_CREATED) {
        showChildWindow();
        return;
    }

    wizViewManager.create(
        //window name should be unique
        'MAIN',

        //configuration of window
        {
            src : 'resources/test.html',
        },

        //success
        function () {
            showChildWindow();
        },

        //failure
        function () {
            alert('fail');
            Ext.Viewport.unmask();
        }
    );

    window.addEventListener('message', function(msg) {
        alert('message!');

        hideChildWindow();
    });

    IS_CREATED = true;
}

Ext.define('HelloWorld.view.Main', {
    extend   : 'Ext.Panel',
    xtype    : 'main',
    requires : [
        'Ext.TitleBar',
        'Ext.Button'
    ],
    config   : {

        items : [
            {
                docked : 'top',
                xtype  : 'titlebar',
                title  : 'wizViewManager'
            },
            {
                xtype   : 'button',
                text    : 'Push Me!',
                handler : function () {
                    Ext.Viewport.mask({
                        xtype   : 'loadmask',
                        message : 'Loading...'
                    });

                    createChildWindow();
                }
            }
        ]
    }
});
